from django.urls import path, include
from landingPage import views

app_name = 'landingPage'

urlpatterns = [
    path('', views.index, name="beranda")
]
